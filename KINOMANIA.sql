create or replace package Kinomania is

count number;
function Функция1(x in varchar2)
RETURN VARCHAR2;
function Функция2(x in int)
RETURN VARCHAR2;

procedure ПРОЦЕДУРА1(x in int, y in int, z in int);
procedure ПРОЦЕДУРА2(x in varchar2);
procedure ПРОЦЕДУРА3(x in int);
procedure ПРОЦЕДУРА4;
procedure ПРОЦЕДУРА5(x in varchar2);
END Kinomania;
