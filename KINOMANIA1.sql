create or replace package body Kinomania is

function �������1(x in varchar2)
RETURN VARCHAR2 
AS 
non exception;
k int;
i VARCHAR2(50);
BEGIN
select Count(id) into k from people
group by id
having id in 
(select id_p from p_f 
inner join films on p_f.id_f = films.id
where films.genre = '�����' and p_f.prof = '���������' 
having Count(id_f) = (select Max(Count(id_f)) from p_f 
inner join films on p_f.id_f = films.id
where films.genre = '�����' and p_f.prof = '���������'
group by id_p)
group by id_p);
if k = 1 then
select distinct id_p into i from p_f 
inner join films on p_f.id_f = films.id
where films.genre = x and p_f.prof = '���������' 
having Count(id_f)= (select Max(Count(id_f)) from p_f 
inner join films on p_f.id_f = films.id
where films.genre = x and p_f.prof = '���������'
group by id_p)
group by id_p;
RETURN i;
else
raise non;
end if;
exception 
WHEN non THEN
raise_application_error(-20001,'�����������, ������� ������������� 
� ���������� ���������� ������� ��������� ����� ���������');
END;

function �������2(x in int)
RETURN VARCHAR2
AS
i VARCHAR2(600):= '������ ����� ���� ��������������������� ������������ ��������� ������. 
���� �� ����������� ������ ������ � ������� �������� ������ � ������ ���������, 
������� ������� � ������ ��� ������ ������ ���� ������ �����, ��� ��������������� � ������������������.';
BEGIN
RETURN i;
END;

procedure ���������1(x in int, y in int, z in int) 
as 
cursor get_inf is 
select name, surname, birth from people
where id in
(select id_p from p_f 
inner join films on p_f.id_f = films.id
where films.rating > z and p_f.prof = '�����'
group by p_f.id_p
having Count(p_f.id_f)>x-1 and Count(p_f.id_f)<y+1);
begin 
for s in get_inf 
loop 
dbms_output.put_line(s.name || ' ' || s.surname || ' ' || s.birth);
end loop; 
end;

procedure ���������2(x in varchar2) 
as 
cursor get_inf is 
select name, surname, birth from people
where id = 
(select distinct id_p from p_f 
inner join films on p_f.id_f = films.id
where films.genre = x and p_f.prof = '���������' 
having Count(id_f)= (select Max(Count(id_f)) from p_f 
inner join films on p_f.id_f = films.id
where films.genre = x and p_f.prof = '���������'
group by id_p)
group by id_p);
begin 
for s in get_inf 
loop 
dbms_output.put_line(s.name || ' ' || s.surname || ' ' || s.birth);
end loop; 
end;

procedure ���������3(x in int) 
as
cursor get_inf is 
select  name, surname, Count(id_f) as k from p_f 
inner join people on people.id = p_f.id_p
inner join films on p_f.id_f = films.id
where p_f.prof = '���������' and year = x
group by name, surname;
begin 
for s in get_inf
loop
dbms_output.put_line(s.name || ' ' || s.surname || ' ' || s.k);
end loop; 
end;

procedure ���������4
as 
cursor get_inf is 
select name, surname, birth from people
where id in
(select distinct id_p from p_f
where p_f.prof = '��������' 
having Count(id_f) > 1
group by id_p)
and id in
(select distinct id_p from p_f
where p_f.prof = '�����' 
having Count(id_f) > 1
group by id_p);
begin 
for s in get_inf 
loop 
dbms_output.put_line(s.name || ' ' || s.surname || ' ' || s.birth);
end loop; 
end;

procedure ���������5(x in varchar2)
as
act EXCEPTION;
i int;
cursor get_inf is
select distinct people.Name, people.surname, people.birth from people
where people.id in 
(select id_p from p_f
where prof = '�����' and id_f in (select films.id from films
where GENRE = x));
begin
select Count(genre) into i from films 
where Genre = x;
if i = 0 then
raise act;
else
for s in get_inf
loop
dbms_output.put_line(s.name || ' ' || s.surname || ' ' || s.birth);
end loop;
end if;
EXCEPTION
WHEN act THEN
dbms_output.put_line('������ ����� ���');
end;
end;