create or replace TRIGGER add_pr
 INSTEAD of insert on PR FOR EACH ROW
 declare
 k int;
 n int;
 z int;
 p int;
 f int;
BEGIN
select Max(people.id) into p from people;
p:=p+1;
select Max(films.id) into f from films;
f:=f+1;
select people.id into k from people, films where people.NAME=:new.NAME and people.SURNAME=:new.SURNAME and 
people.NATIONALITY=:new.NATIONALITY and films.TITLE=:new.TITLE and films.RATING=:new.RATING;
if (k is null) then
select people.id into n from people where people.NAME=:new.NAME and people.SURNAME=:new.SURNAME and 
people.NATIONALITY!=:new.NATIONALITY;
if (n is not null) then
raise_application_error(-20001,'� ������� ��������� ������ ��������������');
end if;
select films.id into z from films where films.TITLE=:new.TITLE and films.RATING!=:new.RATING;
if (z is null) then
raise_application_error(-20002,'� ������� ������ ������ �������');
end if;
INSERT INTO people(people.ID, NAME, SURNAME, NATIONALITY) VALUES (p, :new.NAME, :new.SURNAME, :new.NATIONALITY);
INSERT INTO films(films.id, TITLE, RATING) values (f, :new.TITLE, :new.RATING);
END IF;
END;